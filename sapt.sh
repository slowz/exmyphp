#!/bin/bash

ROOTEMAIL=cmail
apt-get update > /dev/null
toupdate=`apt-get --just-print -V dist-upgrade | grep -i security | grep -i "inst" | cut -d " " -f2 | tr "\n" "%"`
if [ "x${toupdate}" != "x" ]; then
content=`mktemp`
echo -e "\nThe following package must be update:\n" >> ${content}
echo ${toupdate} | tr "%" "\n" >> ${content}
cat ${content} | mail -s "Debian security check - report for `hostname`" ${ROOTEMAIL}
rm ${content}
fi
