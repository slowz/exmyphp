#!/bin/bash
##########################################
# Some code copied from lownendbox script.
# Tested with Debian 7 and 8 32/64bit.
# Script author <kevin@mailhex.com>
# Website https://github.com/slowz/exmyphp
# GPLV3
##########################################

VER="0.3"
>install.log
>install-error.log
exec >  >(tee -a install.log)
exec 2> >(tee -a install-error.log >&2)
source $PWD/options.conf
EPASS=`perl -e 'print crypt("$UPASSWD", "salt"),"\n"'`
# Colors
ESC_SEQ="\x1b["
COL_RESET=$ESC_SEQ"39;49;00m"
COL_RED=$ESC_SEQ"31;01m"
COL_GREEN=$ESC_SEQ"32;01m"
COL_YELLOW=$ESC_SEQ"33;01m"
COL_BLUE=$ESC_SEQ"34;01m"
COL_MAGENTA=$ESC_SEQ"35;01m"
COL_CYAN=$ESC_SEQ"36;01m"
# Gen random string
RAMDOM=`tr -cd '[:alnum:]' < /dev/urandom | fold -w5 | head -n1`
RANSTR=$RANDOM
function check_sanity {
# Do some sanity checking.
	if [ $(/usr/bin/id -u) != "0" ]
		then
		die 'Must be run by root user!'
		fi
		if [ $HASEDIT = 'no' ];
		then
		die "Please edit the options file!"
	fi
## Get Debian version
	if [[ $(cat /etc/debian_version) == *7.* ]];
		then
		DVER="wheezy"
	else
		DVER="jessie"
	fi
		echo $DVER
	}
function die {
	echo "ERROR: $1" > /dev/null 1>&2
    exit 1
}
function get_password() {
# Check whether our local salt is present.
SALT=/var/lib/radom_salt
	if [ ! -f "$SALT" ]
	then
		head -c 512 /dev/urandom > "$SALT"
		chmod 400 "$SALT"
	fi
            password=`(cat "$SALT"; echo $1) | md5sum | base64`
		echo ${password:0:13}
}
function check_install {
	if [ -z "`which "$1" 2>/dev/null`" ]
	then
		executable=$1
		shift
	while [ -n "$1" ]
	do
		DEBIAN_FRONTEND=noninteractive apt-get -q -y install "$1"
		apt-get clean
		print_info "$1 installed for $executable"
		shift
	done
	else
	print_warn "$2 already installed"
	fi
}
function print_info {
	echo -n -e '\e[1;36m'
		echo -n $1
		echo -e '\e[0m'
}
function print_warn {
	echo -n -e '\e[1;33m'
		echo -n $1
		echo -e '\e[0m'
}
function create_database {
	USERID="${USERID:0:15}"
	passwd=`get_password "$USERID@mysql"`
	dbname="${USERID}_$RANDOM"
	mysqladmin create "$dbname"
	echo "GRANT ALL PRIVILEGES ON \`$dbname\`.* TO \`$USERID\`@localhost IDENTIFIED BY '$passwd';" | \
	mysql
cat > "/home/$USERID/.my.cnf" <<END
[mysql]
user = $USERID
password = $passwd
database = $dbname
END
}
function install_nginx {
DEBIAN_FRONTEND=noninteractive apt-get -q -y install  nginx
rm -rf /etc/nginx/nginx.conf
cp $EPATH/config/nginx.conf  /etc/nginx/nginx.conf
}
function install_nginx_site {
mkdir /root/bkup
mv /etc/php5/fpm/pool.d/www.conf /root/bkup/
mkdir /home/$USERID/logs
mkdir -p /home/$USERID/$DOMAIN/public_html
chown www-data:www-data /home/$USERID/logs
cat > "/home/$USERID/$DOMAIN/public_html/index.php" <<END
It Works!
END
echo "<?php phpinfo(); ?>" > /home/$USERID/$DOMAIN/public_html/phpinfo_$RANSTR.php
cp $EPATH/config/vhost.conf  /etc/nginx/sites-enabled/$DOMAIN.conf
sed -i "s/DOMAIN/$DOMAIN/g" /etc/nginx/sites-enabled/$DOMAIN.conf
sed -i "s/USERID/$USERID/" /etc/nginx/sites-enabled/$DOMAIN.conf
cp $EPATH/config/www.conf  /etc/php5/fpm/pool.d/$USERID.conf
cp $EPATH/config/php.conf /etc/nginx/
sed -i 's/^\[www\]$/\['${USERID}'\]/' /etc/php5/fpm/pool.d/$USERID.conf
sed -i 's/^listen =.*/listen = \/var\/run\/php5-fpm-'${USERID}'.sock/' /etc/php5/fpm/pool.d/$USERID.conf
sed -i 's/^user = www-data$/user = '${USERID}'/' /etc/php5/fpm/pool.d/$USERID.conf
sed -i 's/^group = www-data$/group = '${USERID}'/' /etc/php5/fpm/pool.d/$USERID.conf
sed -i 's/listen\.owner = www-data/listen\.owner = '${USERID}'/' /etc/php5/fpm/pool.d/$USERID.conf
sed -i 's/listen\.group = www-data/listen\.group = '${USERID}'/' /etc/php5/fpm/pool.d/$USERID.conf
sed -i 's/^;listen.mode =.*/listen.mode = 0666/' /etc/php5/fpm/pool.d/$USERID.conf
sed -i 's/fastcgi_pass unix:\/var\/run\/php5-fpm\.sock/fastcgi_pass unix:\/var\/run\/php5-fpm-'${USERID}'\.sock/' /etc/nginx/php.conf
chown -R $USERID:$USERID /home/$USERID/$DOMAIN
/etc/init.d/php5-fpm stop
/etc/init.d/php5-fpm start
/etc/init.d/nginx restart
}
function adddomain {
echo -e "Enter Domain name: "
read DOMAIN2
mkdir -p /home/$USERID/$DOMAIN2/public_html
cat > "/home/$USERID/$DOMAIN2/public_html/index.php" <<END
$DOMAIN2 <br>
It Works!
END
chown -R $USERID:$USERID /home/$USERID/$DOMAIN2
cp $EPATH/config/vhost.conf  /etc/nginx/sites-enabled/$DOMAIN2.conf
sed -i "s/DOMAIN/$DOMAIN2/g" /etc/nginx/sites-enabled/$DOMAIN2.conf
sed -i "s/USERID/$USERID/g" /etc/nginx/sites-enabled/$DOMAIN2.conf
/etc/init.d/nginx restart
}
function deldomain {
echo -e "Enter Domain name: "
read DELDOMAIN
rm -rf /etc/nginx/sites-enabled/$DELDOMAIN.conf
rm -rf /home/$USERID/$DELDOMAIN
/etc/init.d/nginx restart
echo "Domain $DELDOMAIN was deleted. But we've saved the log files for the domain'"
}
function secure_mysql {
SECURE_MYSQL=$(expect -c "
set timeout 10
spawn mysql_secure_installation
expect \"Enter current password for root (enter for none):\"
send \"$passwd\r\"
expect \"Change the root password?\"
send \"n\r\"
expect \"Remove anonymous users?\"
send \"y\r\"
expect \"Disallow root login remotely?\"
send \"y\r\"
expect \"Remove test database and access to it?\"
send \"y\r\"
expect \"Reload privilege tables now?\"
send \"y\r\"
expect eof
")

echo "$SECURE_MYSQL"
}
function secure_server {
##sysctl
sed -i 's/^#net.ipv4.conf.all.accept_source_route = 0/net.ipv4.conf.all.accept_source_route = 0/' /etc/sysctl.conf
sed -i 's/^net.ipv4.conf.all.accept_source_route = 1/net.ipv4.conf.all.accept_source_route = 0/' /etc/sysctl.conf
sed -i 's/^#net.ipv6.conf.all.accept_source_route = 0/net.ipv6.conf.all.accept_source_route = 0/' /etc/sysctl.conf
sed -i 's/^net.ipv6.conf.all.accept_source_route = 1/net.ipv6.conf.all.accept_source_route = 0/' /etc/sysctl.conf
sysctl -p
sed -i "s/^Port [0-9]*/Port $SSHP/" /etc/ssh/sshd_config
sed -i "s/#ListenAddress 0.0.0.0/ListenAddress 0.0.0.0:$SSHP/" /etc/ssh/sshd_config
/etc/init.d/ssh restart
## Set cron for security updates and email if needed.
sed -i "s/cmail/$ROOTEMAIL/" ./sapt.sh
mkdir ~/.scripts
cp $EPATH/sapt.sh ~/.scripts/
chmod +x ~/.scripts/sapt.sh
echo "/bin/bash /root/.scripts/sapt.sh" > /etc/cron.daily/lcron
chmod +x /etc/cron.daily/lcron
if [ $SSHKEYONLY = 'yes' ]; then
echo "PasswordAuthentication no" >> /etc/ssh/sshd_config
/etc/init.d/ssh restart
else
echo "ssh password login active."
fi
if [ $IFIREWALL = 'yes' ]; then
DEBIAN_FRONTEND=noninteractive apt-get -q -y install ufw
ufw default deny incoming
ufw default allow outgoing
ufw allow $SSHP
ufw allow www
ufw allow 60000:60100/udp
ufw limit ssh
yes | ufw enable
ufw status
else
echo "ufw not installed."
fi
}
function install_logwatch {
DEBIAN_FRONTEND=noninteractive apt-get -q -y install logwatch libdate-manip-perl libsys-cpuload-perl libsys-cpu-perl
sed -i "s/Output = stdout/Output = mail/" /usr/share/logwatch/default.conf/logwatch.conf
sed -i "s/MailTo = root/MailTo = $ROOTEMAIL/" /usr/share/logwatch/default.conf/logwatch.conf
sed -i "s/Detail = Low/Detail = High/" /usr/share/logwatch/default.conf/logwatch.conf
sed -i "s/MailFrom = Logwatch/MailFrom = Logwatch@$HNAME/" /usr/share/logwatch/default.conf/logwatch.conf
# Allow mail delivery from localhost only
/usr/sbin/postconf -e "inet_interfaces = loopback-only"
}
function tune_php {
	if [ -f /etc/php5/fpm/php.ini ]
	then
# Tweak fpm php.ini
sed -i 's/^max_execution_time.*/max_execution_time = '${PHP_MAX_EXECUTION_TIME}'/' ${PHP_FPM_INI_DIR}
sed -i 's/^memory_limit.*/memory_limit = '${PHP_MEMORY_LIMIT}'/' ${PHP_FPM_INI_DIR}
sed -i 's/^max_input_time.*/max_input_time = '${PHP_MAX_INPUT_TIME}'/' ${PHP_FPM_INI_DIR}
sed -i 's/^post_max_size.*/post_max_size = '${PHP_POST_MAX_SIZE}'/' ${PHP_FPM_INI_DIR}
sed -i 's/^upload_max_filesize.*/upload_max_filesize = '${PHP_UPLOAD_MAX_FILESIZE}'/' ${PHP_FPM_INI_DIR}
sed -i 's/^expose_php.*/expose_php = Off/' ${PHP_FPM_INI_DIR}
sed -i 's/;cgi.fix_pathinfo.*/cgi.fix_pathinfo=0/' ${PHP_FPM_INI_DIR}
sed -i 's/^disable_functions.*/disable_functions = exec,mail,system,passthru,shell_exec,escapeshellarg,escapeshellcmd,proc_close,proc_open,dl,popen,show_source/' ${PHP_FPM_INI_DIR}
cat > /etc/php5/mods-available/apc.ini <<END
extension=apc.so
apc.enabled=1
apc.shm_segments=1
apc.shm_size=32M
apc.ttl=7200
apc.user_ttl=7200
apc.num_files_hint=1024
apc.mmap_file_mask=/tmp/apc.XXXXXX
apc.max_file_size = 1M
apc.post_max_size = 1000M
apc.upload_max_filesize = 1000M
apc.enable_cli=0
apc.rfc1867=0
END
		fi
}
function install_mysql {
# Install the MySQL packages
	check_install mysqld mysql-server
	check_install mysql mysql-client
# Fix for mysql 5.5
		invoke-rc.d mysql stop
		invoke-rc.d mysql start
		passwd=`get_password root@mysql`
		mysqladmin password "$passwd"
cat > ~/.my.cnf <<END
[client]
user = root
password = $passwd
END
	chmod 600 ~/.my.cnf
#echo -e "\e[31;01m `cat ~/.my.cnf`"
	echo -e "${COL_RESET}"
}
function update_timezone {
	echo "$TIMEZ" > /etc/timezone
	dpkg-reconfigure -f noninteractive tzdata
}
function install_prefered {
	echo "postfix postfix/main_mailer_type select Internet Site" | debconf-set-selections
	echo "postfix postfix/mailname string $HNAME" | debconf-set-selections
	echo "postfix postfix/destinations string localhost.localdomain, localhost" | debconf-set-selections
# Install needed packages
	DEBIAN_FRONTEND=noninteractive apt-get -q -y install expect postfix php5-fpm lsb-release man wget dialog sudo curl php5 php5-cgi php5-gd php-apc php5-curl php5-gd php5-intl php5-mcrypt php5-imap php-gettext php5-mysql php5-sqlite php5-cli php-pear sqlite3 php5-imagick $MISC_PACKAGES
	php5enmod pdo
	php5enmod mcrypt
	php5enmod imap
# Disable opcache causing 500 errors
	php5dismod opcache
}
function restartall {
	/etc/init.d/php5-fpm restart
	/etc/init.d/nginx restart
	/etc/init.d/postfix restart
	/etc/init.d/mysql restart
}
function install {
	echo "$HNAME" > /etc/hostname
	echo "127.0.0.1    $HNAME" >> /etc/hosts
	hostname $HNAME
	sysctl kernel.hostname=$HNAME
	/etc/init.d/hostname.sh start
#service networking restart
	apt-get update
	DEBIAN_FRONTEND=noninteractive apt-get -q -y install debconf-utils libc-bin locales apt-utils dialog
	sed -i "s/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/" /etc/locale.gen
	/usr/sbin/locale-gen
	apt-get remove apache2* samba* bind9* mysql-* --purge -y
	apt-get dist-upgrade -
	update_timezone
	install_nginx
	addgroup $USERID
	sleep 1
	useradd -m -p $EPASS $USERID -g $USERID
	sleep 1
	install_prefered
	install_nginx_site
	usermod -a -G www-data $USERID
	tune_php
	/etc/init.d/nginx restart
	install_mysql
	sleep 1
	secure_mysql
	create_database
	update_timezone
	secure_server
	ssh-keygen -f /root/.ssh/id_rsa -t rsa -N ''
	sudo -u $USERID /usr/bin/ssh-keygen -f ~/.ssh/id_rsa -t rsa -N ''
	wget -q http://sourceforge.net/projects/adminer/files/latest/download -O /home/$USERID/$DOMAIN/public_html/adminer_$RANSTR.php
	chown $USERID:$USERID /home/$USERID/$DOMAIN/public_html/adminer_$RANSTR.php
}
function start_exmyphp {
	if [ -f /root/.exmyphp ] ;
	then
	echo -e "${COL_RED}You've already run bash exmyphp.sh install ${COL_RESET}"
	exit
	fi
	touch /root/.exmyphp
	install
	install_logwatch
	restartall
	echo -e "${COL_BLUE} Adminer installed at http://$DOMAIN/adminer_$RANSTR.php"
	echo -e "PHPinfo at http://$DOMAIN/phpinfo_$RANSTR.php"
	echo -e "Domain MySql "
	cat /home/$USERID/.my.cnf
	echo -e "${COL_RESET}"
}
function lversion {
	echo "${COL_BLUE} $VER ${COL_RESET}"
}
##############################################################################################################
check_sanity
case "$1" in
install) start_exmyphp;;
adddomain) adddomain;;
version) lversion;;
*) echo -e "${COL_RED} Sorry, wrong option!"
echo -e ""
for option in install adddomain version
do
echo -e "${COL_BLUE} bash exmyphp.sh $option"
echo -e "${COL_RESET}"
done
;;
esac
export PATH=/bin:/usr/bin:/sbin:/usr/sbin:/usr/local/bin
